> **NOTE:** This project has been moved to [buildgarden/pipelines/helm](https://gitlab.com/buildgarden/pipelines/helm). This version remains here for backwards compatibility.

# Helm Pipeline

[![pipeline status](https://gitlab.com/brettops/pipelines/helm/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/helm/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Test, build, and release Helm charts. This pipeline:

- Lints the chart configuration with
  [helm lint](https://helm.sh/docs/helm/helm_lint/).

- Renders a chart README with
  [helm-docs](https://github.com/norwoodj/helm-docs).

- Publishes the Helm chart to a
  [ChartMuseum](https://chartmuseum.com/)-compatible repository
  ([GitLab Package Registry](https://docs.gitlab.com/ee/user/packages/helm_repository/)
  by default).

## Usage

Include the pipeline:

```yaml
include:
  - project: brettops/pipelines/helm
    file: include.yml
```

Specify `HELM_CHART_PATH` if necessary. This pipeline publishes to the GitLab
Package Registry for the current project by default, using a
[CI job token](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html).

### Using Charts

See the GitLab documentation on [installing Helm
charts](https://docs.gitlab.com/ee/user/packages/helm_repository/#install-a-package).

Currently, only project-level repositories are supported by GitLab, not
group-level.

## Variables

### `HELM_CHART_PATH`

The path of the chart to publish. Defaults to `.`.

### `HELM_CHART_VERSION`:

The chart version to publish. The `version` field in the `Chart.yaml` file
is always overridden by this pipeline. Defaults to `$CI_COMMIT_TAG`.

### `HELM_REPOSITORY_USERNAME`

The username used to push to a ChartMuseum repository. Defaults to
`gitlab-ci-token`.

### `HELM_REPOSITORY_PASSWORD`

The password used to push to a ChartMuseum repository. Defaults to
`$CI_JOB_TOKEN`.

### `HELM_REPOSITORY_URL`

The URL of the ChartMuseum repository to push to. Defaults to
`$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/helm/production`.
